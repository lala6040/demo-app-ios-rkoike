//
//  ViewController.swift
//  DemoApp
//
//  Created by rkoike on 2020/04/09.
//  Copyright © 2020 rkoike. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    let List = ["Picker", "Map", "Video", "Web View", "View Pager", "Form"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) ->
        Int {
        return List.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel!.text = List[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath , animated: true)
        if indexPath.row == 0 {
            self.performSegue(withIdentifier: "toPicker", sender: nil)
        } else if indexPath.row == 1{
            self.performSegue(withIdentifier: "toMap", sender: nil)
        } else if indexPath.row == 2{
            self.performSegue(withIdentifier: "toVideo", sender: nil)
        } else if indexPath.row == 3{
            self.performSegue(withIdentifier: "toWebView", sender: nil)
        } else if indexPath.row == 4{
            self.performSegue(withIdentifier: "toViewPager", sender: nil)
        } else if indexPath.row == 5{
            self.performSegue(withIdentifier: "toForm", sender: nil)
        }
    }
}


