//
//  PickerController.swift
//  DemoApp
//
//  Created by rkoike on 2020/04/10.
//  Copyright © 2020 rkoike. All rights reserved.
//

import UIKit

class PickerController: UIViewController {

    @IBOutlet weak var ColorView: UIView!
    @IBOutlet weak var rSlider: UISlider!
    @IBOutlet weak var gSlider: UISlider!
    @IBOutlet weak var bSlider: UISlider!

    var r = CGFloat(0.0)
    var g = CGFloat(0.0)
    var b = CGFloat(0.0)

    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application

    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }

    @IBAction func sliderAction(_ sender: UISlider) {
        
    }
}
